all: cli webserver

cli:
	jbuilder build src/logarion_cli.exe

webserver:
	jbuilder build src/logarion_webserver.exe

clean:
	jbuilder clean

theme-dark:
	sassc share/sass/main-dark.sass > share/static/main.css

theme-light:
	sassc share/sass/main-light.sass > share/static/main.css

tgz:
	cp _build/default/src/logarion_cli.exe logarion_cli
	cp _build/default/src/logarion_webserver.exe logarion_webserver
	tar czvf "logarion-$(shell date -I)-$(shell git rev-parse --short HEAD).tar.gz" share logarion_cli logarion_webserver
