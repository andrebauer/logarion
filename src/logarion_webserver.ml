let load_file f =
  let ic = open_in f in
  let n = in_channel_length ic in
  let s = Bytes.create n in
  really_input ic s 0 n;
  close_in ic;
  (s)

open Opium.Std

module Lpath = Logarion.Lpath
module Template = Converters.Template

module Configuration = struct
  type t = {
      url      : Uri.t;
      static   : Fpath.t;
      styles   : Fpath.t list;
    }

  let of_config config =
    let open Confix.Config in
    let open Confix.ConfixToml in
    try
      Ok {
          url      = string config ("web"/"url") |> mandatory |> Uri.of_string;
          static   = path  config ("web"/"static_dir") |> mandatory;
          styles   = paths config ("web"/"stylesheets") |> mandatory;
        }
    with Failure str -> Error str

  let validity config =
    let open Confix.Config.Validation in
    empty
    &> is_directory config.static
    &&> files_exist ~parent_dir:config.static config.styles
end

let note_of_body_pairs pairs =
  let open Logarion in
  let note = ListLabels.fold_left ~f:(fun a (k,vl) -> Note.with_kv a (k, List.hd vl) ) ~init:(Note.blank ()) pairs in
  let open Meta in
  let open Date in
  { note with meta = { note.meta with date = { note.meta.date with edited = Some (Ptime_clock.now ()) }}}

let note_of_req req =
  Lwt.map note_of_body_pairs (App.urlencoded_pairs_of_body req)

let string_response s = `String s |> respond'
let html_response   h = `Html h |> respond'
let optional_html_response = function Some h -> html_response h | None -> html_response "Not found"

let serve config_filename =
  let module L = Logarion in
  let module Config = Confix.Config.Make (Confix.ConfixToml) in

  Random.self_init();

  let toml_config =
    let open Confix.Config in
    (if config_filename = "" then Path.with_file ".logarion/config.toml" else Path.of_string config_filename)
    |> function Ok cfg -> Config.from_path cfg | Error str -> prerr_endline str; exit 1
  in
  let config =
    toml_config |> Config.to_record L.Archive.Configuration.of_config
    |> function Ok cfg -> cfg | Error str -> prerr_endline str; exit 1
  in
  let web_config =
    toml_config |> Config.to_record Configuration.of_config
    |> function Ok cfg -> cfg | Error str -> prerr_endline str; exit 1
  in

  Confix.Config.Validation.terminate_when_invalid (Configuration.validity web_config);
  Confix.Config.Validation.terminate_when_invalid (L.Archive.Configuration.validity config);

  let module L = Logarion.Archive.Make(File) in
  let store = File.store config.repository config.extension in
  let lgrn = L.{ config; store; } in

  let blog_url = Uri.to_string web_config.Configuration.url in
  let module Html = Converters.Html in

  let lwt_blanknote () = Lwt.return (Logarion.Note.blank ()) in

  let (>>=) = Lwt.(>>=) and (>|=) = Lwt.(>|=) in
  let atom_response repo req =
    Lwt.return (L.latest_listed repo)
    >|= Converters.Atom.feed config blog_url (L.note_with_id lgrn)
    >>= html_response
  in

  let template_config = toml_config in
  let module T = Converters.Template in
  let header = T.header_converter template_config in
  let body = T.body_converter template_config in
  let linker r x = match Fpath.(relativize ~root:(v r) (v x)) with Some l -> Fpath.to_string l | None -> "" in
  let style = T.default_style in
  let page_of_index metas = T.page_of_index ~style (linker "/") header config metas in
  let page_of_log metas = T.page_of_log ~style (linker "/") header config metas in
  let page_of_note note = T.page_of_note ~style (linker "/note") header body config note in
  let page_of_msg title msg = T.page_of_msg ~style (linker "/note") header config title msg in
  let form_of_note note = T.page style (linker "/") "Write new note" (header (linker "/") config) (Converters.Html.form "" "" note) in

  let post_note lgrn req =
    note_of_req req
    >>= L.with_note lgrn
    >|= page_of_note
    >>= html_response
  in

  let some_note converter par_name lgrn find_note req =
    let basename =
      let open Fpath in
      match of_string (param req par_name) with
      | Ok bn -> to_string (if has_ext ".html" bn then rem_ext bn else bn)
      | Error (`Msg msg) -> prerr_endline msg; par_name
    in
    print_endline @@ "searching for " ^ basename;
    match find_note basename with
    | Some note -> html_response @@ converter note
    | None -> html_response @@ page_of_msg "Not found" "Article not found"
  in
  let edit_note = some_note form_of_note in
  let view_note = some_note page_of_note in

  let index_response param_name lgrn req =
    let n = 128 in
    let from = match Uri.get_query_param (Request.uri req) "p" with
      | Some p -> (try int_of_string p with Failure _ -> 0)
      | None -> 0
    in
    Lwt.return (L.latest_listed lgrn)
    >|= L.sublist ~from:(from * n) ~n
    >|= page_of_index
    >>= html_response
  in
  
  let log_response param_name lgrn req =
    let n = 128 in
    let from = match Uri.get_query_param (Request.uri req) "p" with
      | Some p -> (try int_of_string p with Failure _ -> 0)
      | None -> 0
    in
    Lwt.return (L.latest_listed lgrn)
    >|= L.sublist ~from:(from * n) ~n
    >|= page_of_log
    >>= html_response
  in

  let list_notes_with_topic param_name lgrn req =
    let n = 128 in
    let from = match Uri.get_query_param (Request.uri req) "p" with
      | Some p -> (try int_of_string p with Failure _ -> 0)
      | None -> 0
    in
    Lwt.return (L.with_topic lgrn (param req param_name))
    >|= L.sublist ~from:(from * n) ~n
    >|= page_of_log
    >>= html_response
  in

  let port = match Uri.port web_config.Configuration.url with Some p -> p | None -> 3666 in
  print_endline @@ "Server address: " ^ Uri.to_string web_config.Configuration.url;
  print_endline @@ "Press Ctrl+C to stop.";
  let app =
    App.empty
    |> App.port port
    |> middleware @@
         Middleware.static
           ~local_path:(Fpath.to_string web_config.Configuration.static)
           ~uri_prefix:"/static"
           ()
    |> get "/:ttl"      @@ view_note "ttl" lgrn (L.note_with_alias lgrn)
    |> post "/post.note" @@ post_note lgrn
    |> get "/edit.note/:ttl" @@ edit_note "ttl" lgrn (L.note_with_alias lgrn)
    |> get "/new.note"   (fun _ -> lwt_blanknote () >|= form_of_note >>= html_response)
    |> get "/topic/:topic" @@ list_notes_with_topic "topic" lgrn
    |> get "/note/:ttl" @@ view_note "ttl" lgrn (L.note_with_alias lgrn)
    |> get "/!/:ttl"    @@ view_note "ttl" lgrn (fun t -> match L.latest_entry lgrn t with
                                                          | Some meta -> L.note_with_id lgrn meta.Logarion.Meta.uuid
                                                          | None -> None)
    |> get "/feed.atom" @@ atom_response lgrn
    |> get "/log.html"  @@ log_response "p" lgrn
    |> get "/index.html"@@ index_response "p" lgrn
    |> get "/"          @@ index_response "p" lgrn
    |> App.start
  in
  Lwt_main.run app

let serve_term =
  let open Cmdliner in
  let config = Arg.(value & opt string "" & info ["c"; "config"] ~docv:"CONFIG FILENAME" ~doc:"Configuration filename") in
  Term.(const serve $ config),
  Term.info
    "serve"
    ~doc:"serve repository over web"
    ~man:[ `S "DESCRIPTION";  `P "Launches a webserver for current repository"]

let help_term =
  let open Cmdliner in
  Term.(ret (const (`Help (`Pager, None)))),
  Term.info
    "Logarion webserver" ~version:"0.1.0"
    ~doc:"Logarion repository web server"
    ~man:[ `S "BUGS";
           `P "Submit bugs https://gitlab.com/orbifx/logarion/issues/new."; ]

let cmds = [ serve_term; help_term ]

let () =
  let open Cmdliner in
  match Term.eval_choice serve_term cmds with
  | `Error _ -> exit 1 | _ -> exit 0
