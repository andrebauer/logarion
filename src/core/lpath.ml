open Fpath
type repo_t  = Repo of t
type note_t  = Note of { repo: repo_t; basename: t }

let fpath_of_repo = function Repo p -> p
let string_of_repo r = fpath_of_repo r |> to_string
let repo_of_string s = Repo (v s)

let fpath_of_note = function Note n -> (fpath_of_repo n.repo // n.basename)
let string_of_note n = fpath_of_note n |> to_string
let note_of_basename repo s = Note { repo; basename = v s }

let alias_of_note = function Note n -> n.basename |> rem_ext |> to_string
let note_of_alias repo extension alias = note_of_basename repo (alias ^ extension)

let versioned_basename_of_title ?(version=0) repo extension (title : string) =
  let notes_fpath = fpath_of_repo repo in
  let basename = v @@ Meta.string_alias title in
  let rec next version =
    let candidate = basename |> add_ext (string_of_int version) |> add_ext extension in
    if Sys.file_exists (to_string (notes_fpath // candidate))
    then next (succ version)
    else note_of_basename repo (to_string candidate)
  in
  next version
