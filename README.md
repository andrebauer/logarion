# Logarion

Logarion is a [free and open-source](https://joinup.ec.europa.eu/software/page/eupl) personal note taking, journaling and publication system, a blog-wiki hybrid.

[Screenshots of stock themes](https://gitlab.com/orbifx/logarion/wikis/screenshots).

## Features

Plain file system store (each note is a file).
Command line & web interfaces.

Two publishing modes:

- Static, published upon a command.
  Suitable for scenarios where installation on the server is not possible.

- Dynamic, using web server. 
  Supports interactive features like searching and more advanced Atom feed parameters.

For more features, options and guides visit the [Wiki](https://gitlab.com/orbifx/logarion/wikis).

## Community & support

- Mailing list: <https://lists.orbitalfox.eu/listinfo/logarion>
- Matrix (chat): `#logarion:matrix.org`. Via Riot web-app: <https://riot.im/app/#/room/#logarion:matrix.org>
- For issues peferably email to [mailto:logarion@lists.orbitalfox.eu](mailto:logarion@lists.orbitalfox.eu?subject=[Issue] summary-here).
  Alternatively <https://gitlab.com/orbifx/logarion/issues>

## Install

For development instructions see [CONTRIBUTING.md](CONTRIBUTING.md#developing-contributing).

The following instructions are the quickest way to install Logarion (in the absence of binary releases).

```
opam pin add logarion git://orbitalfox.eu/logarion
opam install logarion
```

Once installed you will have `logarion_cli` for command line control of the repository and `logarion_webserver` for web access.

## Your archive

### Command line

Run `logarion_cli --help`.

A typical initialisation just takes a `logarion_cli init`.
Before running Logarion, configure `.logarion/config.toml`, which is the core configuration file.
The archive options are under the `[archive]` section.

### Web server

The web server's options are under the `[web]` section.
Run `logarion_webserver` and open a browser to <http://localhost:3666>.
To post a new article visit <http://localhost:3666/new.note> (**Note**: there is no authorisation control).

#### Theme

Optionally install a [Sass](http://sass-lang.com/) compiler, like [sassc](http://sass-lang.com/libsass#sassc), and then run `make theme-dark` or `make theme-light`, to generate a stylesheet as `share/static/main.css`, using the respective Sass files in `share/sass/`.

## See also

- [CONTRIBUTING.md](CONTRIBUTING.md)
- [Licence](https://joinup.ec.europa.eu/software/page/eupl)
